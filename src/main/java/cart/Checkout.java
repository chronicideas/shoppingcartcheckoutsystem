package cart;

import items.Fruit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Checkout {

    private HashMap<Fruit, Integer> cartItems = new LinkedHashMap<>();

    int add(int quantity, Fruit fruit) {
        int existingQty = 0;
        if (cartItems.get(fruit) != null) existingQty = cartItems.get(fruit) + quantity;
        else {
            cartItems.put(fruit, existingQty + quantity);
            existingQty = cartItems.get(fruit);
        }
        return existingQty;
    }

    int update(int quantity, Fruit fruit) {
        cartItems.put(fruit, quantity);
        return cartItems.get(fruit);
    }

    Integer delete(Fruit fruit) {
        cartItems.remove(fruit);
        return cartItems.get(fruit);
    }

    String getTotal() {
        double cost = 0;
        for (Map.Entry<Fruit, Integer> cartItem : cartItems.entrySet()) {
            cost = cost + cartItem.getValue() * cartItem.getKey().getCost();
        }

        NumberFormat decimalFormat = DecimalFormat.getCurrencyInstance(Locale.UK);
        return decimalFormat.format(cost);
    }
}

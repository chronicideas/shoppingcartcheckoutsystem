package items;

public class Fruit {

    private double cost;

    public Fruit(double cost) {
        this.cost = cost;
    }

    public double getCost() {
        return cost;
    }
}

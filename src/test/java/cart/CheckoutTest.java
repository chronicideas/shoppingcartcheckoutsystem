package cart;

import items.Fruit;
import org.junit.Test;

import static org.junit.Assert.*;

public class CheckoutTest {

    private Checkout checkout = new Checkout();
    private Fruit apple = new Fruit(0.60);
    private Fruit orange = new Fruit(0.25);

    @Test
    public void testAdd() {
        assertEquals(2, checkout.add(2, apple));
        assertEquals(5, checkout.add(3, apple));
    }

    @Test
    public void testUpdate() {
        checkout.add(5, orange);
        checkout.add(10, orange);
        assertEquals(3, checkout.update(3, orange));
        assertEquals(2, checkout.update(2, orange));
    }

    @Test
    public void testDelete() {
        checkout.add(3, apple);
        checkout.delete(apple);
        assertNull(checkout.delete(apple));
    }

    @Test
    public void testGetTotal() {
        checkout.add(5, apple);
        assertEquals("£3.00", checkout.getTotal());
        checkout.add(3, orange);
        assertEquals("£3.75", checkout.getTotal());
        checkout.update(1, apple);
        assertEquals("£1.35", checkout.getTotal());
        checkout.delete(apple);
        assertEquals("£0.75", checkout.getTotal());
        checkout.delete(orange);
        assertEquals("£0.00", checkout.getTotal());
    }
}
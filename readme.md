# Introduction

This is a simple checkout system for a shopping cart. It does not contain any user interface and is simply the business logic along with appropriate unit tests.

# Testing

Either run the individual unit tests in an IDE or in CLI run ```./gradlew test``` or simply ```gradle test```